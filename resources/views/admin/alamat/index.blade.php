@extends('admin.layouts.master')
 
@section('content')
 
<div class="row">
    <div class="col-md-12">

        <h4>{{ $title }}</h4>
        <div class="box box-warning">
            <div class="box-header">
                <!-- <p>
                    <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                </p> -->
            </div>
            <div class="box-body">

                <div class="row">
                    <div class="col-md-6">
                    <table class="table table-hover table-striped">
                        <tbody>
                        @foreach( $alamat as $row )
                            <tr>
                                <th>Provinsi</th>
                                <td>:</td>
                                <td>{{ $row->provinsi }}</td>
                            </tr>
                            <tr>
                                <th>Kota</th>
                                <td>:</td>
                                <td>{{ $row->kota }}</td>
                            </tr>
                            <tr>
                                <th>Tanggal Buat </th>
                                <td>:</td>
                                <td>{{ $row->created_at }}</td>
                            </tr>
                            <tr>
                                <th>Tanggal Update</th>
                                <td>:</td>
                                <td>{{ $row->update_at }}</td>
                            </tr>
                            
                            @endforeach
                        </tbody>
                        </table>
                    </div>

                    <div class="col-md-6">
                        <form role="form" method="post" action="{{ url('alamat') }}">
                            @csrf
                            <div class="box-body">
                                
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Provinsi</label>
                                    <select name="provinsi" class="form-control select2">
                                        <option>Pilih Provinsi</option>
                                        @foreach( $provinsi->rajaongkir->results as $pr )
                                        <option value="{{ $pr->province_id }}">{{ $pr->province }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Kota/Kabupaten</label>
                                    <select name="kota" class="form-control select2">
                                        
                                    </select>
                                </div>
                
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 
@endsection
 
@section('scripts')
 
<script type="text/javascript">
    $(document).ready(function(){

        $("select[name='provinsi']").change(function(e){
            $("select[name='kota']").empty();
            var id = $(this).val();
            var url = "{{ url('alamat/get-kota') }}"+'/'+id;
            
            $.ajax({
                type: 'get',
                dataType:'json',
                url:url,
                success:function(data){
                    console.log(data.data);

                    var hasil = '';
                    $.each(data.data.rajaongkir.results, function(i,v){
                        hasil += '<option value="'+v.city_id+'">';
                            hasil += v.type + ' ' + v.city_name;
                        hasil += '</option>';
                    });

                    $("select[name='kota']").append(hasil);
                }
            });

            
        });
 
        // btn refresh
        $('.btn-refresh').click(function(e){
            e.preventDefault();
            $('.preloader').fadeIn();
            location.reload();
        })
 
    })
</script>
 
@endsection