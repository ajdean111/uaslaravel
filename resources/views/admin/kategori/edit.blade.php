@extends('admin.layouts.master')
 
@section('content')
 
<div class="row">
    <div class="col-md-12">
        <h4>{{ $title }}</h4>
        <div class="box box-warning">
            <div class="box-header">
                <!-- <p>
                    <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                </p> -->
            </div>
            <div class="box-body">
               
                <form role="form" method="post" action="{{ url('kategori/'.$data->id) }}">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama Kategori</label>
                            <input type="text" name="kategori" class="form-control" id="exampleInputEmail1" placeholder="Input Kateogri" value="{{ $data->kategori }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Kode</label>
                            <input type="Kode" name="kode" class="form-control" id="exampleInputPassword1" placeholder="Input Kode Kategori" value="{{ $data->kode }}">
                        </div>
                    </div>
                    <!-- /.box-body -->
        
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
 
@endsection
 
@section('scripts')
 
<script type="text/javascript">
    $(document).ready(function(){
 
        // btn refresh
        $('.btn-refresh').click(function(e){
            e.preventDefault();
            $('.preloader').fadeIn();
            location.reload();
        })
 
    })
</script>
 
@endsection