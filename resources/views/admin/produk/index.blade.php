@extends('admin.layouts.master')
 
@section('content')
 
<div class="row">
    <div class="col-md-12">
        <h4>{{ $title }}</h4>
        <div class="box box-warning">
            <div class="box-header">
                <p>
                    <!-- <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button> -->
                    <a href="{{ url('/produk/add') }}" class="btn btn-sm btn-flat btn-primary"><i class="fa fa-plus"></i> Tambah Kategori</a>
                </p>
            </div>
            <div class="box-body">
               
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Berat</th>
                            <th>Kategori</th>
                            <th>Harga</th>
                            <th>Stok</th>
                            <th>Minimal Stok</th>
                            <th>Tanggal Buat </th>
                            <th>Tanggal Update</th>
                            <th>Photo</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $i=1;
                        @endphp
                        @foreach( $data as $row )
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $row->kode }}</td>
                            <td>{{ $row->produk }}</td>
                            <td>{{ $row->berat }} gr</td>
                            <td>{{ $row->kategori->kategori }}</td>
                            <td>{{ $row->harga }}</td>
                            <td>{{ $row->stok }}</td>
                            <td>{{ $row->minimal_stok }}</td>
                            <td>{{ $row->created_at }}</td>
                            <td>{{ $row->updated_at }}</td>
                            <td><img src="{{ $row->photo }}" alt="" style="width : 120px;"></td>
                            <td>
                                <a href="{{ url('produk/'.$row->id) }}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                                <a href="{{ url('produk/'.$row->id) }}" class="btn btn-sm btn-danger btn-hapus"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
 
@endsection
 
@section('scripts')
 
<script type="text/javascript">
    $(document).ready(function(){
 
        // btn refresh
        $('.btn-refresh').click(function(e){
            e.preventDefault();
            $('.preloader').fadeIn();
            location.reload();
        })
 
    })
</script>
 
@endsection