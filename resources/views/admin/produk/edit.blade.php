@extends('admin.layouts.master')
 
@section('content')
 
<div class="row">
    <div class="col-md-12">
        <h4>{{ $title }}</h4>
        <div class="box box-warning">
            <div class="box-header">
                <!-- <p>
                    <button class="btn btn-sm btn-flat btn-warning btn-refresh"><i class="fa fa-refresh"></i> Refresh</button>
                </p> -->
            </div>
            <div class="box-body">
               
                <form role="form" method="post" action="{{ url('produk/'.$data->id) }}" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama Produk</label>
                            <input type="text" name="produk" class="form-control" id="exampleInputEmail1" placeholder="Input Produk" value="{{ $data->produk }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Kode Produk</label>
                            <input type="text" name="kode" class="form-control" id="exampleInputPassword1" placeholder="Input Kode Produk" value="{{ $data->kode }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Berat (gram)</label>
                            <input type="number" name="berat" class="form-control" id="exampleInputPassword1" placeholder="Input berat produk (gram)" value="{{ $data->berat }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Kategori</label>
                            <select name="kategori_id" class="form-control select2">
                                <option>Pilih Kategori</option>
                                @foreach( $kategori as $kt )
                                <option value="{{ $kt->id }}" {{ ($data->kategori_id == $kt->id ) ? 'selected' : '' }}>{{ $kt->kategori }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Harga</label>
                            <input type="number" name="harga" class="form-control" id="exampleInputPassword1" value="{{ $data->harga }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Stok</label>
                            <input type="number" name="stok" class="form-control" id="exampleInputPassword1" value="{{ $data->stok }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Minimal Stok</label>
                            <input type="number" name="minimal_stok" class="form-control" id="exampleInputPassword1" value="{{ $data->minimal_stok }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Upload Photo</label>
                            <input type="file" name="photo">
                        </div>
                    </div>
                    <!-- /.box-body -->
        
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
 
@endsection
 
@section('scripts')
 
<script type="text/javascript">
    $(document).ready(function(){
 
        // btn refresh
        $('.btn-refresh').click(function(e){
            e.preventDefault();
            $('.preloader').fadeIn();
            location.reload();
        })
 
    })
</script>
 
@endsection