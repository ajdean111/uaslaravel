@extends('layouts.master')

@section('content')

<table class="table"> 
    <thead>
        <tr>
            <th>Nama Produk</th>
            <th>Qty</th>
            <th>Berat</th>
            <!-- <th>Total Berat</th> -->
            <th>Harga</th>
            <!-- <th>Total Harga</th> -->
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        @php 
            $total_berat = 0;
        @endphp
        @foreach($data as $dt)
        <tr>
            <td>{{ $dt['name'] }}</td>
            <td>{{ $dt['quantity'] }}</td>
            <!-- <td>{{ $dt['attributes']['berat'] }}</td> -->
            <td>{{ $dt['attributes']['berat'] * $dt['quantity'] }}</td>
            @php 
                $total_berat += $dt['attributes']['berat'] * $dt['quantity'];
            @endphp
            <td>Rp. {{ number_format($dt['price'],0) }}</td>
            <!-- <td>Rp. {{ $dt['price'] * $dt['quantity'] }}</td> -->
            <td>
                <a href="{{ url('front/hapus-cart/'.$dt->id) }}" class="cart-hapus btn btn-sm btn-danger">Hapus</a>
            </td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td><b>Total</b></td>
            <td><b>{{ $total_qty }}</b></td>
            <td><b>{{ $total_berat }} gr</b></td>
            <td><b>Rp. {{ number_format($subTotal,0) }}</b></td>
            
        </tr>
    </tfoot>
</table>

<table>
    <thead>
        <tr>
            <th>Pilih Provinsi</th>
            <th>Pilih Kota/Kabupaten</th>
            <th>Pilih Kurir</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <select name="provinsi" class="form-control">
                    <option selected="" disabled="">Pilih Provinsi</option>
                    @foreach($provinsi->rajaongkir->results as $pr)
                    <option value="{{ $pr->province_id }}">{{ $pr->province }}</option>
                    @endforeach
                </select>
            </td>
            <td>
                <select name="kota" class="form-control">
                    <option selected="" disabled="">Pilih Provinsi</option>
                </select>
            </td>

            <td>
                <select name="kurir" class="form-control">
                    <option selected="" disabled="">Pilih Kurir</option>
                    <option value="jne">jne</option>
                    <option value="tiki">tiki</option>
                    <option value="pos">pos</option>
                </select>
            </td>

            <td>
                <button class="btn btn-cek-ongkir btn-primary">Cek Ongkir</button>
            </td>
        </tr>

        <tr>
            <td class="list-ongkir">

            </td>
        </tr>
    </tbody>
</table>

@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){

        $('.btn-cek-ongkir').click(function(e){
            e.preventDefault();
            $('.list-ongkir').empty();

            var kota_asal = "{{ $kota_asal }}";
            var kota_tujuan = $("select[name='kota']").val();
            var kurir = $("select[name='kurir']").val();
            var berat = "{{ $total_berat }}";

            var url = "{{ url('front/cek-ongkir') }}"+'/'+kota_asal+'/'+kota_tujuan+'/'+berat+'/'+kurir;

            $.ajax({
                type:'get',
                dataType:'json',
                url:url,
                success:function(data){
                    console.log(data);

                    var ongkirs = '';

                    $.each(data.data.rajaongkir.results, function(i,v){

                        $.each(v.costs, function(a,b){
                            // ongkirs += b.service + ' ' + b.cost[0].value;
                            ongkirs += '<input type="radio" name="servis">' +b.service + ' ' + b.cost[0].value+'<br>';
                        });
                        
                    })

                    $('.list-ongkir').append(ongkirs);
                }
            })
        })
        
        $("select[name='provinsi']").change(function(){
            $("select[name='kota']").empty();
            var provinsi = $(this).val();
            var url = "{{ url('front/get-kota') }}"+'/'+provinsi;

            $.ajax({
                type:'get',
                dataType:'json',
                url:url,
                success:function(data){
                    console.log(data.data.rajaongkir.results);

                    var kotas = '';
                    $.each(data.data.rajaongkir.results, function(i,v){
                        kotas += '<option value="'+v.city_id+'">';
                            kotas += v.type + ' ' + v.city_name;
                        kotas += '</option>'
                    });

                    $("select[name='kota']").append(kotas);
                }
            })
        })
    })
</script>

@endsection