<div id="sidebar" class="span3">
		<div class="well well-small"><a id="myCart" href="product_summary.html"><img src="{{ asset('bootshop/themes/images/ico-cart.png')}}" alt="cart">3 Items in your cart  <span class="badge badge-warning pull-right">$155.00</span></a></div>
		<ul id="sideManu" class="nav nav-tabs nav-stacked">
			

			@php
				$kategori = \App\Models\Kategori::orderBy('kategori','asc')->get();
			@endphp

			@foreach($kategori as $kt)
				<li><a href="{{ url('/front/kategori/'.$kt->id) }}">{{ $kt->kategori }} [{{$kt->produks->count()}}]</a></li>
			@endforeach

		</ul>
		<br/>

		<!-- FUNGSI MENAMPILKAN DATA SECARA RANDOM -->
		<!-- @php
			$randoms = App\Models\Produk::limit(2)->inRandomOrder()->get();
		@endphp

		@foreach( $randoms as $rd )
		  <div class="thumbnail">
			<img src="{{ asset($rd->photo) }}" alt="{{ $rd->produk }}"/>
			<div class="caption">
			  <h5>{{ $rd->produk }}</h5>
				<h4 style="text-align:center"><a class="btn" href="{{ url('front/produk/'.$rd->id) }}"> <i class="icon-zoom-in"></i></a> <a class="btn" href="#">Add to <i class="icon-shopping-cart"></i></a> <a class="btn btn-primary" href="#">Rp. {{ number_format($rd->harga,0) }}</a></h4>
			</div>
		  </div><br/>
		@endforeach -->


		
			<!-- <div class="thumbnail">
				<img src="{{ asset('bootshop/themes/images/payment_methods.png')}}" title="Bootshop Payment Methods" alt="Payments Methods">
				<div class="caption">
				  <h5>Payment Methods</h5>
				</div>
			  </div> -->
	</div>