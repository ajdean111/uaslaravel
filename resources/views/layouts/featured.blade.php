<div class="row-fluid">
    <div id="featured" class="carousel slide">
    <div class="carousel-inner">

        <div class="item-active">
            <ul class="thumbnails">
            @php 
                $featureds = \App\Models\Produk::where('is_featured', 1)->get();
            @endphp

            @foreach($featureds as $ft)
                <li class="span3">
                    <div class="thumbnails">
                        <i class="tag"></i>
                        <a href=""><img src="{{ asset($ft->photo) }}" alt=""></a>
                        <div class="caption">
                            <h5>{{ $ft->produk }}</h5>
                            <h4><a class="btn" href="">VIEW</a><span class="pull-right">{{ $ft->harga }}</span></h4>
                        </div>
                    </div>
                </li>
            @endforeach    
            </ul>
        </div>

        <div class="item">
        <ul class="thumbnails">
        <li class="span3">
            <div class="thumbnail">
            <a href="product_details.html"><img src="{{ asset('bootshop/themes/images/products/2.jpg')}}" alt=""></a>
            <div class="caption">
                <h5>Product name</h5>
                <h4><a class="btn" href="product_details.html">VIEW</a> <span class="pull-right">$222.00</span></h4>
            </div>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail">
            <a href="product_details.html"><img src="{{ asset('bootshop/themes/images/products/3.jpg')}}" alt=""></a>
            <div class="caption">
                <h5>Product name</h5>
                <h4><a class="btn" href="product_details.html">VIEW</a> <span class="pull-right">$222.00</span></h4>
            </div>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail">
            <a href="product_details.html"><img src="{{ asset('bootshop/themes/images/products/4.jpg')}}" alt=""></a>
            <div class="caption">
                <h5>Product name</h5>
                <h4><a class="btn" href="product_details.html">VIEW</a> <span class="pull-right">$222.00</span></h4>
            </div>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail">
            <a href="product_details.html"><img src="{{ asset('bootshop/themes/images/products/5.jpg')}}" alt=""></a>
            <div class="caption">
                <h5>Product name</h5>
                <h4><a class="btn" href="product_details.html">VIEW</a> <span class="pull-right">$222.00</span></h4>
            </div>
            </div>
        </li>
        </ul>
        </div>
        </div>
        <a class="left carousel-control" href="#featured" data-slide="prev">‹</a>
        <a class="right carousel-control" href="#featured" data-slide="next">›</a>
        </div>
        </div>