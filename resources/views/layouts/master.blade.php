<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layouts.head')
  </head>
<body>


@include('layouts.menubar')


<!-- Header End====================================================================== -->
<!-- <div id="carouselBlk">
	<div id="myCarousel" class="carousel slide">
		<div class="carousel-inner">
		  <div class="item active">
		  <div class="container">
			<a href="register.html"><img style="width:100%" src="{{ asset('bootshop/themes/images/carousel/1.png')}}" alt="special offers"/></a>
			<div class="carousel-caption">
				  <h4>Second Thumbnail label</h4>
				  <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
				</div>
		  </div>
		  </div>
		  <div class="item">
		  <div class="container">
			<a href="register.html"><img style="width:100%" src="{{ asset('bootshop/themes/images/carousel/2.png')}}" alt=""/></a>
				<div class="carousel-caption">
				  <h4>Second Thumbnail label</h4>
				  <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
				</div>
		  </div>
		  </div>
		  <div class="item">
		  <div class="container">
			<a href="register.html"><img src="{{ asset('bootshop/themes/images/carousel/3.png')}}" alt=""/></a>
			<div class="carousel-caption">
				  <h4>Second Thumbnail label</h4>
				  <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
				</div>
			
		  </div>
		  </div>
		   <div class="item">
		   <div class="container">
			<a href="register.html"><img src="{{ asset('bootshop/themes/images/carousel/4.png')}}" alt=""/></a>
			<div class="carousel-caption">
				  <h4>Second Thumbnail label</h4>
				  <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
				</div>
		   
		  </div>
		  </div>
		   <div class="item">
		   <div class="container">
			<a href="register.html"><img src="{{ asset('bootshop/themes/images/carousel/5.png')}}" alt=""/></a>
			<div class="carousel-caption">
				  <h4>Second Thumbnail label</h4>
				  <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
			</div>
		  </div>
		  </div>
		   <div class="item">
		   <div class="container">
			<a href="register.html"><img src="{{ asset('bootshop/themes/images/carousel/6.png')}}" alt=""/></a>
			<div class="carousel-caption">
				  <h4>Second Thumbnail label</h4>
				  <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
				</div>
		  </div>
		  </div>
		</div>
		<a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
		<a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
	  </div> 
</div> -->
<div id="mainBody">
	<div class="container">
	<div class="row">

<!-- Sidebar ================================================== -->
@include('layouts.sidebar')
<!-- Sidebar end=============================================== -->


		<div class="span9">		
			<div class="well well-small">
			<h4>Produk Unggulan <small class="pull-right">200+ featured products</small></h4>
            
            @include('layouts.featured')

		</div>
		<h4>Produk </h4>
              
            @yield('content')

		</div>
		</div>
	</div>
</div>
<!-- Footer ================================================================== -->
    
    @include('layouts.footer')

<!-- Placed at the end of the document so the pages load faster ============================================= -->
	<script src="{{ asset('bootshop/themes/js/jquery.js')}}" type="text/javascript"></script>
	<script src="{{ asset('bootshop/themes/js/bootstrap.min.js')}}" type="text/javascript"></script>
	<script src="{{ asset('bootshop/themes/js/google-code-prettify/prettify.js')}}"></script>
	
	<script src="{{ asset('bootshop/themes/js/bootshop.js')}}"></script>
    <script src="{{ asset('bootshop/themes/js/jquery.lightbox-0.5.js')}}"></script>
	
	<!-- Themes switcher section ============================================================================================= -->
<div id="secectionBox">
<link rel="stylesheet" href="{{ asset('bootshop/themes/switch/themeswitch.css')}}" type="text/css" media="screen" />
<script src="{{ asset('bootshop/themes/switch/theamswitcher.js')}}" type="text/javascript" charset="utf-8"></script>
	<div id="themeContainer">
	<div id="hideme" class="themeTitle">Style Selector</div>
	<div class="themeName">Oregional Skin</div>
	<div class="images style">
	<a href="themes/css/#" name="bootshop"><img src="{{ asset('bootshop/themes/switch/images/clr/bootshop.png')}}" alt="bootstrap business templates" class="active"></a>
	<a href="themes/css/#" name="businessltd"><img src="{{ asset('bootshop/themes/switch/images/clr/businessltd.png')}}" alt="bootstrap business templates" class="active"></a>
	</div>
	<div class="themeName">Bootswatch Skins (11)</div>
	<div class="images style">
		<a href="themes/css/#" name="amelia" title="Amelia"><img src="{{ asset('bootshop/themes/switch/images/clr/amelia.png')}}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="spruce" title="Spruce"><img src="{{ asset('bootshop/themes/switch/images/clr/spruce.png')}}" alt="bootstrap business templates" ></a>
		<a href="themes/css/#" name="superhero" title="Superhero"><img src="{{ asset('bootshop/themes/switch/images/clr/superhero.png')}}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="cyborg"><img src="{{ asset('bootshop/themes/switch/images/clr/cyborg.png')}}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="cerulean"><img src="{{ asset('bootshop/themes/switch/images/clr/cerulean.png')}}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="journal"><img src="{{ asset('bootshop/themes/switch/images/clr/journal.png')}}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="readable"><img src="{{ asset('bootshop/themes/switch/images/clr/readable.png')}}" alt="bootstrap business templates"></a>	
		<a href="themes/css/#" name="simplex"><img src="{{ asset('bootshop/themes/switch/images/clr/simplex.png')}}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="slate"><img src="{{ asset('bootshop/themes/switch/images/clr/slate.png')}}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="spacelab"><img src="{{ asset('bootshop/themes/switch/images/clr/spacelab.png')}}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="united"><img src="{{ asset('bootshop/themes/switch/images/clr/united.png')}}" alt="bootstrap business templates"></a>
		<p style="margin:0;line-height:normal;margin-left:-10px;display:none;"><small>These are just examples and you can build your own color scheme in the backend.</small></p>
	</div>
	<div class="themeName">Background Patterns </div>
	<div class="images patterns">
		<a href="themes/css/#" name="pattern1"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern1.png') }}" alt="bootstrap business templates" class="active"></a>
		<a href="themes/css/#" name="pattern2"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern2.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern3"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern3.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern4"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern4.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern5"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern5.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern6"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern6.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern7"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern7.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern8"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern8.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern9"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern9.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern10"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern10.png') }}" alt="bootstrap business templates"></a>
		
		<a href="themes/css/#" name="pattern11"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern11.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern12"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern12.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern13"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern13.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern14"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern14.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern15"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern15.png') }}" alt="bootstrap business templates"></a>
		
		<a href="themes/css/#" name="pattern16"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern16.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern17"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern17.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern18"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern18.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern19"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern19.png') }}" alt="bootstrap business templates"></a>
		<a href="themes/css/#" name="pattern20"><img src="{{ asset('bootshop/themes/switch/images/pattern/pattern20.png') }}" alt="bootstrap business templates"></a>
		 
	</div>
	</div>
</div>
<span id="themesBtn"></span>

<script type="text/javascript">
	$(document).ready(function(){
		$('.cart-hapus').click(function(e){
			var conf = confirm('Yakin akan dihapus?');
			if(conf){

			}else{
				e.preventDefault();
			}
		})
	})
</script>

@yield('scripts')

</body>
