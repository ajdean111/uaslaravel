<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('cek-cart', function(){
    $cek = \Cart::getContent();
    //dd($cek);
});

Route::get('/', 'Beranda_controller@index');

Route::get('/keluar', function(){
    \Auth::logout();
    return redirect('/login');
});

Auth::routes();

Route::get('/home', function(){
    return redirect('/admin');
});

Route::prefix('front')->group(function(){
    Route::get('kategori/{id}','Kategori_controller@index');

    Route::get('produk/search','Produk_controller@search');
    //detail produks
    Route::get('produk/{id}','Produk_controller@detail');

    //cart
    Route::get('add-cart/{id}','Cart_controller@add');
    Route::get('detail-cart','Cart_controller@detail');
    Route::get('hapus-cart/{id}','Cart_controller@destroy');

    Route::get('get-kota/{id_provinsi}','Cart_controller@get_kota_ajax'); 
    Route::get('cek-ongkir/{kota_asal}/{kota_tujuan}/{berat}/{kurir}','Cart_controller@get_ongkir'); 

});

Route::group(['middleware' => 'auth'], function(){
    
    Route::get('/admin','Admin\Beranda_controller@index');

    // MASTER KATEGORI ================================================
    Route::get('/kategori','Admin\Kategori_controller@index')->name('kategori');
    //create
    Route::get('/kategori/add','Admin\Kategori_controller@create');
    Route::post('/kategori/add','Admin\Kategori_controller@store')->name('kategori_add');
    //edit
    Route::get('/kategori/{id}','Admin\Kategori_controller@edit');
    Route::put('/kategori/{id}','Admin\Kategori_controller@update');
    //delete
    Route::delete('/kategori/{id}','Admin\Kategori_controller@delete');

    // MASTER PRODUK ====================================================
    Route::get('/produk','Admin\Produk_controller@index');
    //create
    Route::get('/produk/add','Admin\Produk_controller@add');
    Route::post('/produk/add','Admin\Produk_controller@store');
    //edit
    Route::get('/produk/{id}','Admin\Produk_controller@edit');
    Route::put('/produk/{id}','Admin\Produk_controller@update');
    //delete
    Route::delete('/produk/{id}','Admin\Produk_controller@delete');

    //PRODUK UNGGULAN
    Route::get('/featured-produk','Admin\Produk_controller@featured');
    Route::put('/featured-produk','Admin\Produk_controller@featured_update');


    Route::get('alamat','Admin\Alamat_controller@index');
    Route::get('alamat/get-kota/{id_provinsi}','Admin\Alamat_controller@get_kota_ajax');
    Route::post('alamat','Admin\Alamat_controller@store');

});


