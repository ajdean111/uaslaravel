<?php

use Illuminate\Database\Seeder;

class ProdukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $produk = [
            'produk'    => 'Laptop',
            'kode'      => 'KD-P001',
            'harga'     => '24000000',
            'stok'      => '5',
            'created_at'=> now()
        ];

        DB::table('produk')->insert($produk);
    }
}
