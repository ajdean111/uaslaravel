<?php

use Illuminate\Database\Seeder;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kategori = [
            'kategori'  => 'Elektronik',
            'kode'      => 'KD-K001',
            'created_at'=> now()
        ];

        DB::table('kategori')->insert($kategori);
    }
}
