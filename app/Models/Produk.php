<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk';
    protected $fillable = ['kategori_id', 'produk', 'kode', 'berat', 'is_featured'];

    public function kategori()
    {
        return $this->belongsTo('App\Models\Kategori','kategori_id','id');
    }
}
