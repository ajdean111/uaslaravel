<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Produk;
use App\Models\Kategori;

class Produk_controller extends Controller
{
    public function index()
    {
        $title = 'Data Produk';
        $data = Produk::orderBy('produk','asc')->get();

        return view('admin.produk.index', compact('title','data'));
    }

    public function add()
    {
        $title = 'Tambah Produk';
        $kode = 'KD-P';
        $kategori = Kategori::get();

        return view('admin.produk.create', compact('title', 'kode','kategori'));
    }

    public function store(Request $request)
    {
        $file = $request->file('photo');
        if($file){
            $nama_file = $file->getClientOriginalName();
            $file->move('img/produk_images', $nama_file);
            $photo = 'img/produk_images/'.$nama_file;
        }else{
            $photo = null;
        }

        $data = new Produk;
        $data->produk       = $request->produk;
        $data->kategori_id  = $request->kategori_id;
        $data->kode         = $request->kode;
        $data->berat        = $request->berat;
        $data->harga        = $request->harga;
        $data->stok         = $request->stok;
        $data->minimal_stok = $request->minimal_stok;
        $data->photo        = $photo;
        $data->save();

       // \Session::flash('sukses','Data produk berhasil ditambahkan');
        return redirect('/produk');
    }

    public function edit($id)
    {
        $title = 'Edit Data Produk';
        $data = Produk::find($id);
        $kategori = Kategori::get();

        return view('admin.produk.edit', compact('title','data','kategori'));
    }

    public function update(Request $request, $id)
    {
        $row = Produk::find($id);

        $file = $request->file('photo');
        if($file){
            $nama_file = $file->getClientOriginalName();
            $file->move('img/produk_images', $nama_file);
            $photo = 'img/produk_images/'.$nama_file;
        }else{
            $photo = $row->photo;
        }

        $data = Produk::find($id);
        $data->produk       = $request->produk;
        $data->kategori_id  = $request->kategori_id;
        $data->kode         = $request->kode;
        $data->berat        = $request->berat;
        $data->harga        = $request->harga;
        $data->stok         = $request->stok;
        $data->minimal_stok = $request->minimal_stok;
        $data->photo        = $photo;
        $data->save();

       // \Session::flash('sukses','Data produk berhasil diubah');
        return redirect('/produk');
    }

    public function delete($id)
    {
        $data = Produk::where('id',$id)->delete();
        
       // \Session::flash('sukses', 'Data produk berhasil dihapus!');
        return redirect('/produk');
    }

    public function featured()
    {
        $title = 'List Fatured Produk';
        $produks = Produk::get();
        $featured = Produk::where('is_featured',1)->get();

        return view('admin.produk.featured', compact('title', 'produks', 'featured'));
    }

    public function featured_update(Request $request)
    {
        $id = $request->produk;

        $cek = Produk::find($id);
        if($cek->is_featured == 1){
            $dt = Produk::find($id);
            $dt->is_featured = null;
            $dt->save();
        }else{
            $dt = Produk::find($id);
            $dt->is_featured = 1;
            $dt->save();
        }

        return redirect()->back();//->with('sukses', 'Data barhasil disimpan');
    }
}
