<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Kategori;

class Kategori_controller extends Controller
{
    public function index()
    {
        $title = 'List Kategori';
        $data = Kategori::orderBy('kategori','asc')->get();

        return view('admin.kategori.index', compact('title', 'data'));
    }

    public function create()
    {
        $title = 'Tambah Kategori';
        $kode = 'KD-K';

        return view('admin.kategori.create', compact('title', 'kode'));
    }

    public function store(Request $request)
    {
        $data = new Kategori;
        $data->kategori = $request->kategori;
        $data->kode     = $request->kode;
        $data->save();

       // \Session::flash('sukses','Data kategori berhasil ditambahkan');

        return redirect('/kategori');
    }

    public function edit($id)
    {
        $title = 'Edit Kategori';
        $data  = Kategori::find($id);

        return view('admin.kategori.edit', compact('title', 'data'));
    }

    public function update(Request $request, $id)
    {
        $data = Kategori::find($id);
        $data->kategori = $request->kategori;
        $data->kode = $request->kode;
        $data->save();

       // \Session::flash('sukses', 'Data kategori berhasil diupdate');
        return redirect('/kategori');
    }

    public function delete($id)
    {
        $data = Kategori::find($id)->delete();

       // \Session::flash('sukses', 'Data berhasil dihapus');
        return redirect('/kategori');
    }
}
