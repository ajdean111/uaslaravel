<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;
use App\Models\Alamat;

class Cart_controller extends Controller
{
    public function add($id)
    {
        $produk = Produk::find($id);

        $cek = \Cart::get($id);
        if($cek == null){
            \Cart::add([
                'id' => $id,
                'name' => $produk->produk,
                'price' => $produk->harga,
                'quantity' => 1,
                'attributes' => ['berat' => $produk->berat]
            ]);
        }else{
            $qty_now = $cek->quantity;
            $new_qty = $qty_now+1;
            \Cart::update($id,['quantity'=>[
                'relative'  => false,
                'value'     => $new_qty,
                ]]);
        }

        return redirect()->back()->with('pesan', 'Data berhasil dimaksukan ke keranjang');
    }

    public function detail()
    {
        $data = \Cart::getContent();
        $total_qty = \Cart::getTotalQuantity();
        $subTotal = \Cart::getSubTotal();
        $provinsi = $this->get_provinsi();

        $alamat = Alamat::first();
        $kota_asal = $alamat->kota;
        // dd($data[6]['attributes']['berat']);
        // dd($provinsi->rajaongkir->results);

        return view('beranda.cart', compact('data', 'total_qty', 'subTotal', 'provinsi', 'kota_asal'));
    }

    public function get_ongkir($kota_asal, $kota_tujuan, $berat, $kurir){
        $curl = curl_init();
 
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "origin=$kota_asal&destination=$kota_tujuan&weight=$berat&courier=$kurir",
        CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key: 34359148d4677db6f216643375042d76"
        ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
        // echo "cURL Error #:" . $err;
        } else {
        // echo $response;
        }

        return response()->json([
            'data' => json_decode($response)
        ]);

    }

    public function get_provinsi()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key: 34359148d4677db6f216643375042d76"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            // echo "cURL Error #:" . $err;
        } else {
            // echo $response;
        }

        return json_decode($response);
    }

    public function get_kota_ajax($id_provinsi)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.rajaongkir.com/starter/city?province=$id_provinsi",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "key: 34359148d4677db6f216643375042d76"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        // echo "cURL Error #:" . $err;
        } else {
        // echo $response;
        }

        return response()->json([
            'data' =>json_decode($response)
        ]);
    }

    public function destroy($id)
    {
        \Cart::remove($id);

        return redirect()->back();
    }
}
