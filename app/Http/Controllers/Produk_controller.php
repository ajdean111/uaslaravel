<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;

class Produk_controller extends Controller
{
    public function detail($id)
    {
        $dt = Produk::find($id);

        return view('beranda.produk_detail', compact('dt'));
    }

    public function search(Request $request)
    {
        $keyword = $request->keyword;
        $kategori_id = $request->kategori_id;

        if($kategori_id == 'all'){
            $latests = \App\Models\Produk::where('produk','like','%'. $keyword .'%')->latest()->get();
        }else{
            $latests = \App\Models\Produk::where('produk','like','%'. $keyword .'%')->where('kategori_id', $kategori_id)->latest()->get();
        }

        

        return view('beranda.index', compact('latests'));
    }
}
