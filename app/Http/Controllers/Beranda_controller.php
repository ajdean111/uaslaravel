<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Beranda_controller extends Controller
{
    public function index()
    {
        $latests = \App\Models\Produk::latest()->get();

        return view('beranda.index', compact('latests'));
    }
}
