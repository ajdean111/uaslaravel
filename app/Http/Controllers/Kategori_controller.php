<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;
use App\Models\Produk;

class Kategori_controller extends Controller
{
    public function index($id)
    {
        $latests = \App\Models\Produk::where('kategori_id',$id)->latest()->get();

        return view('beranda.kategori',compact('latests'));
    }
}
